**************************************************************************************************************************************************************************************
** PURPOUSE                                                                                                                                                                         **
**************************************************************************************************************************************************************************************
    Application to periodically check for weather updates for a location specified by configuration file.


**************************************************************************************************************************************************************************************
** DESCRIPTION                                                                                                                                                                      **
**************************************************************************************************************************************************************************************
    The process resembles the Producer-Consumer pattern, where one module produces weather data
to be stored locally (on shared memory block) and the other attempts to read said block of data and assess 
if certain conditions are met or not.
(if any parameter values stored in shared memory block like temperature, humidity, wind, etc.
have exceeded a variation quantum threshold)

    In the former case, an email is sent to a subscribed recipient address, stating which parameter values
have exceeded assigned threshold.
    Both sender and recipient credentials, as well as values for parameter thresholds and target location
will be passed by configuration files, one for each module of the application.


**************************************************************************************************************************************************************************************
** DISCLAIMER                                                                                                                                                                       **
**************************************************************************************************************************************************************************************
    This application was developed as proof of minimum required technical skill for 
the open position of Junior Software Developer at AMD Romania and is not considered a stable software utility.
    Use at your own risk.
