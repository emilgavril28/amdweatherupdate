import smtplib
from email.mime.text import MIMEText

import configparser

import platform
import os
import pdb


class MailingController:
    """
    Class containing all necessary functionality for sending basic text emails,
    with recipient and sender address, as well as gmail application passwords being read from
    a configuration file. As such, only a configuration file path is required to initialise
    this type of object
    """

    def __init__(self, configFilePath):
        """
        Constructor, initialising all necessary data (credentials and email server name)
        for sending emails successfully

        :param self          : reference to parent object
        :param configFilePath: file path of configuration file containing data for this object
        :return              : nothing! exceptional use cases will be raised as errors
        """
        confParse = configparser.ConfigParser()
        if len(confParse.read(configFilePath)) != 0:
            raise FileNotFoundError("""The path you have provided is invalid.
            Please provide a valid mail service configuration file path
            to be able to use this application.""")

        # For the moment being, this application may only communicate between Google Mail accounts
        self.smtpServer = "smtp.gmail.com"
        try:
            self.senderAddr = confParse["sender"]["address"]
            self.senderPass = {
                "Windows": confParse["sender"]["passWin"],
                "Linux": confParse["sender"]["passLin"],
                "Darwin": confParse["sender"]["passMac"]
            }
            self.recipientAddr = confParse["recipient"]["address"]

        except KeyError:
            print("""Insufficient information provided to configure SMTP client.
            Please make sure to append all necessary credentials to configuration file 
            before calling the constructor.""")

        PASS = self.senderPass
        # If, among the read passwords, none is >= 8 characters
        if len([PASS[elem] for elem in PASS if len(PASS[elem]) > 8]) < 1:
            print("""WARNING: None of the provided application passwords is valid.
            Please obtain a valid Google Mail application password
            (one for each OS on which you intend to run this application)
            then add them to the mailing configuration file before calling this constructor.""")

    def initServerConn(self) -> bool:
        """
        Function to check if connection to the server has been successfully established

        :param self: reference to parent object
        :return    : truth/boolean value, showing if the connection was successfully established
        """
        self.connection = smtplib.SMTP(host=self.smtpServer, port=587)

        try:
            # Ask server to use extended SMTP protocol, TLS encryption protocol (secure) and
            # automatically initiate connection (send an EHLO message command for identification)
            self.connection.starttls()
            # Login using application password specific for this OS, if possible
            self.connection.login(self.senderAddr, self.senderPass[platform.system()])

        except KeyError:
            print(f"""No gmail app password provided for running this app on {platform.system()}.
            Please obtain a gmail app password suitable for this operating system and try again.""")

            return False

        except smtplib.SMTPAuthenticationError as serr:
            print("ERROR: " + str(serr.smtp_error))
            print()
            return False

        # Server rejected connection for some reason
        except smtplib.SMTPHeloError as serr:
            print("ERROR: " + str(serr.smtp_error))
            return False

        return True

    def sendMail(self, subject: str = "Generic mail",
                 message: str = "This is an automated email. Hello!") -> bool:
        """
        Function to check if connection to the server has been successfully established

        :param self   : reference to parent object
        :param subject: subject of email message to send
        :param message: actual content/description of email to be sent
        :return       : truth/boolean value, showing if the message was successfully sent
        """
        if self.initServerConn():
            # Initialize Multipurpose Internet Mail Extension doc object
            msgObj = MIMEText(message)
            msgObj["Subject"] = subject

            try:
                self.connection.sendmail(self.senderAddr, self.recipientAddr, msgObj.as_string())

            except smtplib.SMTPSenderRefused:
                print("ERROR: Sender address refused by server. Function will exit...")
                return False

            except smtplib.SMTPRecipientsRefused:
                print("""ERROR: Given recipient addresses were refused by the server.
                Function will exit...""")
                return False

            self.connection.quit()
            return True

        return False
