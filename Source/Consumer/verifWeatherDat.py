import configparser
import re

import sys
import os
import time
import pdb

sys.path.insert(1, os.path.abspath("../Helper"))
from rotateLogHelper import createRotLogger, writeToLog, timeStructToStr
from weathRWHelper import readWeatherData

sys.path.insert(1, os.path.abspath("SMTP//"))
from client import MailingController

# Create logger with scope of entire file
LOGOBJ = createRotLogger()[0]
if not LOGOBJ:
    print("WARNING: Logger could not be initialised.")

else:
    writeToLog(LOGOBJ, "Rotating logger initialised successfully.")

EMPTYFLAG = 'r'

TRSHTEMP_DEFAULT = 8
TRSHUMID_DEFAULT = 20


def formatMessageData(currStr: str, lastStr: str, thresh: dict) -> str:
    """
    Function to format message meta data as a simple paragraph + table string, then return it

    :param currStr: string containing formatted data of currently read weather data sample
    :param lastStr: string containing formatted data of last read weather data sample
	:param thresh : dictionary{int} containing variation threshold values for temperature. humidity
    :return       : formatted string, to appear as an email message if printed
    """
    currVal = extractDataFrom(currStr)
    lastVal = extractDataFrom(lastStr)

    # Create list containing values of
    # both (current and last) weather data samples for temperature and humidity,
    # cast to string, with each 1 digit value having a '0' added in front of it
    valStrList = [((2 - len(str(elem))) * '0' + str(elem)) for elem in
                  [currVal["temperature"],
                   currVal["humidity"],
                   lastVal["temperature"],
                   lastVal["humidity"]]]
    # Create list containing truth values for given thresholds, cast to string,
    # with each "No" value having a trailing space added to it
    thrFlagList = [(3 - len(str(elem))) * '0' + str(elem) for elem in
                   [abs(currVal["temperature"] - lastVal["temperature"]) > thresh["temperature"],
                    abs(currVal["humidity"] - lastVal["humidity"]) > thresh["humidity"]]]

    # The pattern we are searching for is equivalent to "Temperature: Value measurementUnit"
    DEGREES = "°" + re.search("T:\d{1,2}[ckf]", currStr)[0][-1].upper()

    messageHead = f"""Hello Sir/Madam,\n\n
    This is an automated message sent from a Python script regarding your concern for
    the current weather variation in {currVal['location']}.\n\n"""
    tHeadLine = 50 * " " + "||   Temp       ||   Humid     ||" + 33 * " " + "\n"
    spaceLine = 88 * " " + "\n"
    currLine = f"""Current Weather Data Sample
        ||     {valStrList[0]} {DEGREES}     
        ||     {valStrList[0]} %     ||\n"""
    lastLine = f"""Last Weather Data Sample
        ||     {valStrList[2]} {DEGREES}     
        ||     {valStrList[3]} %     ||\n"""
    threshLine = f"""Threshold Exceeded
        ||      {thrFlagList[0]}      
        ||     {thrFlagList[1]}    ||\n\n"""
    messageFoot = "We hope the provided information was helpful and we wish you a good day.\n\n"
    messageGreeting = "Sincerely,\nWeather Info Team"

    messageTable = tHeadLine + spaceLine + currLine + spaceLine + lastLine + spaceLine + threshLine
    return messageHead + messageTable + messageFoot + messageGreeting


def sendSMTPWeatherUpdate(currStr: str, lastStr: str, thresh: dict) -> bool:
    """
    Function to send a weather update email to recipient address subscribed to this application

    :param currStr: string containing formatted data of currently read weather data sample
    :param lastStr: string containing formatted data of last read weather data sample
	:param thresh : dictionary{int} housing variation threshold values for temperature, humidity
    :return       : truth value, dependent on wether the message was sent successfully or not
    """
    # The configuration file passed as parameter to the constructor
    # contains credentials of sender, recipient
    client = MailingController(os.path.abspath("SMTP//conf.ini"))
    return client.sendMail("Weather Notification Service v.ALPHA (for CD with Jenkins)",
                           formatMessageData(currStr, lastStr, thresh))


def extractDataFrom(dataStr: str) -> dict:
    """
	Function to recieve data string (formatted for direct memory storage)
	and return it as dictionary containing values for temperature and humidity, respectively

	:param dataStr: string containing concisely formatted data of location,temperature and humidity
    :return       : a dictionary containing the int/str values of said parameters
    """
    try:
        tempVal = int(re.search("T:\d{1,2}c", dataStr)[0][2: -1])
        humidVal = int(re.search("H:\d{1,2}p", dataStr)[0][2: -1])
        locStr = re.search("[\w\s\-]{1,73},[\w\s\-]{1,73},[\w\s\-]{1,73}", dataStr)[0].replace(",", ", ")
        return {"humidity": humidVal, "temperature": tempVal, "location": locStr}

    except IndexError:
        writeToLog(LOGOBJ, """ERROR: Weather data string incorrectly formatted.
        Data string should be of form 'T:{val}c|f|k; H:{val}p;'. Data not extracted.""")
        return {}

    except TypeError:
        writeToLog(LOGOBJ, "ERROR: No weather data to could be extracted.")
        return {}


def checkWeatherVariation(currStr: str, lastStr: str, thresh: dict) -> bool:
    """
	Function to check if one or both variation thresholds have been exceeded.

    :param currStr: string containing formatted data of currently read weather data sample
    :param lastStr: string containing formatted data of last read weather data sample
	:param thresh : dictionary{str} housing variation threshold values for temperature, humidity
    :return       : truth value, dependent on wether any of two thresholds has been exceeded
    """
    try:
        currVal = extractDataFrom(currStr)
        lastVal = extractDataFrom(lastStr)

        if currVal and lastVal:
            return (abs(currVal["temperature"] - lastVal["temperature"]) >
                    int(thresh["temperature"])) or \
                   (abs(currVal["humidity"] - lastVal["humidity"]) >
                    int(thresh["humidity"]))

        return False

    except KeyError:
        writeToLog(LOGOBJ, """ERROR: In consequence of ill formatted input data,
                        degree of variation could not be computed.""")
        return False


def runWeatherCheck(thresh: dict = {"temperature": TRSHTEMP_DEFAULT, "humidity": TRSHUMID_DEFAULT},
                    lastVal: str = "") -> list | bool:
    """
	Function to check if weather variation thresholds have been exceeded and, if necessary,
	send a notification email to any email adresses subscribed to this it by configuration file.

	:param thresh    : dictionary{str|int} housing variation threshold vals for temperature, humidity
	:param lastVal   : concisely formatted string version of last read weather data sample
    :return          : list of format [ currWeatherData, shouldNotifyFlag ], or truth value
    """
    try:
        currVal = readWeatherData(sys.argv[1])
        if currVal != EMPTYFLAG and len(currVal):
            writeToLog(LOGOBJ, f"New data sample successfully read (value is '{currVal}').")

        else:
            writeToLog(LOGOBJ, "No new data sample could be read from pointed memory block.")
            return False

        thresh["temperature"] = int(thresh["temperature"])
        thresh["humidity"] = int(thresh["humidity"])

    except IndexError:
        writeToLog(LOGOBJ, """ERROR: No memory block tag was provided.
        Memory block cannot be accessed for writing without first knowing its name/tag.
        You must make sure a memory block tag is passed as CLI argument to this app.""")
        raise IndexError("""No memory block tag was provided.
        Memory block cannot be accessed for writing without first knowing its name/tag.\n
        You must make sure a memory block tag is passed as CLI argument to this app.""")

    except ValueError:
        writeToLog(LOGOBJ, """ERROR :Given threshold values are invalid.
        A variation threshold in range [1,100] must be assigned to each parameter
        (temperature, humidity).
        Threshold parameters will revert to default values.""")
        thresh = {"temperature": TRSHTEMP_DEFAULT, "humidity": TRSHUMID_DEFAULT}

    threshExceedFlag = checkWeatherVariation(currVal, lastVal, thresh)
    if lastVal not in [None, ""] and threshExceedFlag:
        writeToLog(LOGOBJ, """Weather parameters variation registered.
                            Preparing to send notification email...""")

        if sendSMTPWeatherUpdate(currVal, lastVal, thresh):
            writeToLog(LOGOBJ, "Email message successfully sent.")

        else:
            writeToLog(LOGOBJ, "Email message could not be sent. Please try again later.")

    else:
        writeToLog(LOGOBJ,
                "No significant change in weather status detected, no notification email sent. ")

    return [currVal, threshExceedFlag]


if __name__ == "__main__":
    writeToLog(LOGOBJ, """Script to read weather data from memory space and check if weather
    (temperature, humidity) data variation exceeds thresholds (specified in configuration file),
    then send a notification email accordingly, started!""")
    print(f"Started {timeStructToStr(time.localtime())}\n\n")

    confParse = configparser.ConfigParser()
    confParse.read(os.path.abspath("conf.ini"))

    paramKeys = list(confParse["threshold"].keys())
    # If configuration file cotains
    # both of the conf parameters required for weather variation identification
    # (temperature, humidity, in this order)
    if ["temperature", "humidity"] == paramKeys:
        with open(os.path.abspath("./SMTP/hidden/last_sample.txt"), "r+") as file:
            lastValStr = file.readline()
            if lastValStr not in ["", None]:
                print(f"Last stored value: {lastValStr}")
                result = runWeatherCheck(dict(confParse["threshold"]), lastValStr)

            else:
                print("No previously store value found.")
                result = runWeatherCheck(confParse["threshold"])

        # If function returned without errors and a new data sample has been parsed in the process
        if result and result[0]:
            currValStr = (str(result[0][:-1]))
            with open("./SMTP/hidden/last_sample.txt", "w+") as file:
                file.write(currValStr)

            writeToLog(LOGOBJ, "Process exited normally.")

    else:
        writeToLog(LOGOBJ, """ERROR: Configuration file incomplete,
        with insufficient information provided regarding thresholds,
        or invalid, with provided parameters incorrectly formatted
        (thresholds are provided as TempThr, HumidThr).
        Please make necessary changes before attempting to run the program again.""")
        writeToLog(LOGOBJ, "Process exited with errors.")
else:
    writeToLog(LOGOBJ, """WARNING: Calling this program from another python module is forbidden.
    This program should be run only as a topmost module.""")
