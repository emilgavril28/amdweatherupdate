from multiprocessing.shared_memory import SharedMemory

import sys
import os
import time
import pdb


def attachToMemBlock(memBlockTag: str) -> SharedMemory | bool:
    """
	Function to return attachment to a shared memory block identifiable
	by memBlockTag parameter

    :param memBlockTag : name/tag of shared memory block to read data from
    :return            : attachment to shared memory object pointed by tag
    """
    try:
        attachShMem = SharedMemory(name=memBlockTag, create=False, size=128)
        print("Existing memory block found.")

    except NameError:
        raise NameError("""No memory block tag was provided.
        Memory block canot be accessed without first knowing its name/tag.""")

    except FileNotFoundError:
        print("No existing block found,\n")
        if os.name == "nt":
            print("Unfortunately, you are currently running this script on a Windows machine.")
            print("""As such, without the use of a 3rd party library,
            a shared memory block cannot be initialised from it.""")
            print("Function exited with errors.")
            return False

        print("Creating one right away.")
        attachShMem = SharedMemory(name=memBlockTag, create=True, size=128)

    return attachShMem
