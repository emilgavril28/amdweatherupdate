import sys
import os
import time
import pdb

from memAccHelper import attachToMemBlock


def writeWeatherData(memBlockTag: str, dataStr: str) -> bool:
    """
	Function to write data string recieved as parameter to shared memory block with name memBlockTag

    :param memBlockTag: name/tag of shared memory block to which data is supposed to be appended
    :param dataStr    : string containing information to write to shared memory block
    :return           : truth value, showing if data was successfully written to memory or not
    """
    attachShMem = attachToMemBlock(memBlockTag)
    if attachShMem:
        attachShMem.buf[: len(dataStr)] = bytes(dataStr, encoding="utf-8")
        attachShMem.buf[len(dataStr): 128] = bytes("\x00" * (128 - len(dataStr)), encoding="utf-8")
        attachShMem.close()
        return True

    return False


def readWeatherData(memBlockTag: str, nullByte: int = 0) -> str | bool:
    """
	Function to read and return data stored on shared memory block with name memBlockTag

    :param memBlockTag : name/tag of shared memory block from which data is supposed to be read
    :param nullByte    : byte value ([0,255]) signifying null value
    :return            : read weather data string or truth value, on faliure
    """
    attachShMem = attachToMemBlock(memBlockTag)
    if attachShMem:
        messageStr = "".join([chr(elem) for elem in attachShMem.buf if elem != nullByte])
        attachShMem.close()
        return messageStr

    print("No existing memory block found with given tag name.")
    return False
