import logging
from logging import Logger
from logging.handlers import RotatingFileHandler

import sys
import os
import time
import pdb


def writeToLog(logger: Logger, message: str):
    """
	Function to format and write notifications/warnings/error to log file

    :param logger : logging.Logger object, implementing rotation log capabilities
    :param message: string containing information to write in log
    :return       : nothing! logger object validity should be checked before calling this function
    """
    logger.info(f"({timeStructToStr(time.localtime())})  {message}")
    print(f"({timeStructToStr(time.localtime())})  {message}")


def createRotLogger() -> list | bool:
    """
	Function to initialize a rotating logging object with defined log file path

    :return: truth value, dependent on wether the logger was successfully initialized or not
    """
    filePath = os.path.abspath("../Log/log")
    try:
        logger = logging.getLogger()
        logger.setLevel(logging.INFO)

        # Add rotating behaviour
        handler = RotatingFileHandler(filePath, maxBytes=50, backupCount=10)
        logger.addHandler(handler)

        return [logger, handler]

    except:
        print("Rotating handler could not be created for said log file path")
        return False


def timeStructToStr(timeStruct: time.struct_time = None) -> str:
    """
	Function to convert time data from coposite obj to primitive str

    :param timeStruct : time structure containing data to be converted to string
    :return           : concisely formatted string containing mm:dd:yyyy, hh:mm:ss data
    """
    return f"""on {str(timeStruct.tm_mday)}/{str(timeStruct.tm_mon)}/{str(timeStruct.tm_year)},
                at {str(timeStruct.tm_hour)}:{str(timeStruct.tm_min)}:{str(timeStruct.tm_sec)}"""
