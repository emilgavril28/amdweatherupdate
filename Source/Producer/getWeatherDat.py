import asyncio
from aiohttp import ClientSession, ClientTimeout, TCPConnector
from aiohttp.client_exceptions import ClientConnectorError
from urllib.parse import quote_plus
from socket import gaierror

import configparser

import sys
import os
import time
import pdb

sys.path.insert(1, os.path.abspath("../Helper"))
from rotateLogHelper import createRotLogger, writeToLog, timeStructToStr
from weathRWHelper import writeWeatherData

# Create logger with scope of entire file
LOGOBJ = createRotLogger()[0]
if not LOGOBJ:
    print("WARNING: Logger could not be initialised.")

else:
    writeToLog(LOGOBJ, "Rotating logger initialised successfully.")


async def callWeathAPI(locStr: str) -> str | bool:
    """
    This function initializes local web client on the go which it then uses for requesting
    weather data specific to parameter given location.

    :param locStr: pretty printed location string (to be formatted and embedded into the link)
    :return      : string containing concisely formatted weather data or boolean value, on failure
    """
    # Here, set waiting period is probably 5 seconds
    client = ClientSession(timeout=ClientTimeout(total=5000.0), connector=TCPConnector(ssl=False))
    try:
        # quote_plus function is supposed to format given location name to URL valid form
        async with client.get(f"https://wttr.in/{quote_plus(locStr)}?format=j1") as response:
            dataDict = await response.json()

            tempC = dataDict["current_condition"][0]["temp_C"]
            humid = dataDict["current_condition"][0]["humidity"]

            locationDict = dataDict["nearest_area"][0]
            formLocStr = locationDict["country"][0]["value"] + "," + \
                         locationDict["region"][0]["value"] + "," + \
                         locationDict["areaName"][0]["value"]

            if locStr.split(", ")[1] == locationDict["country"][0]["value"]:
                writeToLog(LOGOBJ, "Provided location has been found.")
                return f"T:{tempC}c;H:{humid}p;{formLocStr};"

            writeToLog(LOGOBJ, """Provided location could not be found,
                            showing results for found location anyway.""")
            return False

    except ClientConnectorError:
        writeToLog(LOGOBJ, "ERROR: No internet connection found\nData not parsed.")
        return False

    except gaierror:
        writeToLog(LOGOBJ, "ERROR: Connection could not be established, data not parsed.")
        return False

    finally:
        await client.close()


async def getCurrWeather(cityStr: str = None, countryStr: str = None):
    """
    This function fetches weather data from an API for parameter given location
    and stores it in shared memory block.
    (if memory block tag/name is given as script argument)

    :param cityStr   : string containing name of target location city
    :param countryStr: string containing name of target location country
    :return          : nothing! we have no interest in returning anything
    """
    fullLocStr = f"{cityStr}, {countryStr}"
    if fullLocStr != ", ":
        formWeathStr = await callWeathAPI(fullLocStr)
        if formWeathStr:
            try:
                # At this point it is assumed that received data is valid
                # and can be safely stored on shared memory block
                if writeWeatherData(sys.argv[1], formWeathStr):
                    writeToLog(LOGOBJ, "Fetched weather data successfully written to memory block.")

                else:
                    writeToLog(LOGOBJ, """ERROR: No weather data found for provided location.
                    Please make sure given location data is correct and try again later.""")

            except IndexError:
                writeToLog(LOGOBJ, """ERROR: No memory block tag was provided.
                Memory block cannot be accessed for writing without first knowing its name/tag.
                You must make sure a memory block tag is passed as CLI argument to this app.""")
                raise IndexError("""No memory block tag was provided.
                Memory block cannot be accessed for writing without first knowing its name/tag.\n
                You must make sure a memory block tag is passed as CLI argument to this app.""")

    else:
        writeToLog(LOGOBJ,
                   "ERROR: No location data provided to this function. Data cannot be parsed.")
        raise NameError("No location data provided to this function.\nData cannot be parsed\n")


if __name__ == "__main__":
    writeToLog(LOGOBJ, "Script to parse weather data for location (read from conf file) started!")
    print(f"Started {timeStructToStr(time.localtime())}\n")

    confParse = configparser.ConfigParser()
    confParse.read(os.path.abspath("conf.ini"))

    paramKeys = list(confParse["location"].keys())
    # If configuration file contains both location parameters,
    # accessible by the keywords ('city', 'country', in this order)
    if ["city", "country"] == paramKeys:
        confCountry = confParse["location"]["country"]
        confCity = confParse["location"]["city"]

        asyncio.run(getCurrWeather(confCity, confCountry))
        writeToLog(LOGOBJ, "Process exited normally.")

    else:
        print(paramKeys)
        writeToLog(LOGOBJ, """ERROR: Configuration file incomplete,
        with insufficient information provided regarding location,
        or invalid, with provided parameters incorrectly formatted (location data is provided as City, Country).
        Please make necessary changes before attempting to run the program again.""")
        writeToLog(LOGOBJ, "Process exited with errors.")

else:
    writeToLog(LOGOBJ, """WARNING: Calling this program from another python module is forbidden.
    This program should be run only as a topmost module.""")
